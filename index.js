
/*

models - WHAT, objects

controllers - contains instructions on HOW your API will be controlled, mongoose model queries are used here

routes = defines WHEN particular controllers will be used

*/


// setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes");

// server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Database Connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin123:admin123@project0.641hnql.mongodb.net/s36?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
	}
);


let db = mongoose.connection;

db.on("error", console.error.bind(console,"Connection error."));

db.once("open",()=>console.log("We're connected to the cloud database."));

app.use("/tasks", taskRoutes);


app.listen(port,() => console.log(`Server running at port ${port}`));