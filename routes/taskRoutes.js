
const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskControllers");

// Routes
// routes are responsible for  defining the URIs that our client accesses and the corresponding controller functions that will be used when our route is accessed


// Route to get all the tasks
// this route expects to receive a GET request at the URL "/tasks"
// the whole URL is at "http://localhost:3001"
router.get("/", (req,res)=>{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

router.post("/",(req,res)=>{
	taskController.createTask(req.body).then(resultFromController=>res.send(resultFromController));
})

// route to delete a task
// expects tp receive a DELETE request at the URL "tasks/:id"
// the whole URL is http://localjpst:3001/tasks/:id
router.delete("/:id",(req,res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController=>res.send(resultFromController));
})


router.put("/:id",(req,res)=>{
	taskController.updateTask(req.params.id,req.body).then(resultFromController=>res.send(resultFromController));
})


// Activity s36

router.get("/:id", (req,res)=>{
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete",(req,res)=>{
	taskController.updateStatus(req.params.id,req.body).then(resultFromController=>res.send(resultFromController));
})






// use "module.exports" to export the router object to use in the index.js
module.exports = router;